#Retrieve dataset's Metadata information (given the dataset code)

#Required libraries
library(ipchem.dataretrieval)
library(magrittr)
library(stringr)
library(rjson)

#1. User code knows already the datasets (e.g. 'EFSAMOPER','EFSAVMPR'),
#or it can retrieve the lists of possible values by using the specific helper functions:

#a. the full list of datasets
retrieve_dataset()

#b. the list of dataset filtered by chemical, media, location and/or period
#(e.g. media is "Food and Feed")
retrieve_dataset(media = "Food and Feed")

#3. User code requests metadata for the desired dataset
meta_list <- retrieve_meta(c("EFSAMOPER","EFSAVMPR"))

#Note: the metadata information can be saved in a list and JSON file
meta_list <- retrieve_meta(c("EFSAMOPER","EFSAVMPR"))
meta_file = toJSON(meta_list)

write(meta_file, ".\\metadata.json")
